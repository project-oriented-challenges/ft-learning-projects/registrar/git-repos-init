#!/usr/bin/env python

import csv

# This script assumes that the CSV file with the project data exists
# in the current directory.
# The script will produce the shell file containing the steps to
# intialized all projects listed in the CSV data.

#META_GROUP="onti-2019-fintech/"
META_GROUP=""

exec_file = "#!/bin/bash\n\n"

with open('repos.csv') as csvfile:
    d = csv.reader(csvfile, delimiter=";")
    for row in d:
        print(row)
        gr   = row[0]
        i    = row[1]

        if len(META_GROUP) != 0:
            git_path = f'{META_GROUP}{gr}/{i}'
        else:
            git_path = f'{gr}/{i}'
        #git_path = "group-to-test-and-remove"

        exec_file+=f'echo "generate repo for {i}"\n'
        exec_file+="cp -a template repo\n"
        exec_file+="cd repo\n"
        exec_file+="git init\n"
        exec_file+=f'git remote add origin git@gitlab.com:{git_path}/registrar.git\n'
        exec_file+=f'git submodule add -b develop git@gitlab.com:{git_path}/contract.git contracts\n'
        exec_file+="mv .gitmodules .gitmodules.tmp\n"
        exec_file+="head -2 .gitmodules.tmp > .gitmodules\n"
        exec_file+='echo "        url = ../contract.git" >> .gitmodules\n'
        exec_file+="tail -1 .gitmodules.tmp >> .gitmodules\n"
        exec_file+="rm .gitmodules.tmp\n"
        exec_file+="git add .\n"
        exec_file+="git commit -m 'Initial commit from template [skip ci]'\n"
        exec_file+="git push -u origin master\n"
        exec_file+="cd ..\n"
        exec_file+="rm -rf repo\n\n"
    
    with open('init-repos.sh', 'w') as script_file:
        script_file.write(exec_file)
