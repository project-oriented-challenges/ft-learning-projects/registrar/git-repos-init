#!/usr/bin/env python

from web3 import Web3, HTTPProvider
from json import load

with open('account.json') as file:
    account_config = load(file)

with open('database.json') as file:
    database = load(file)

with open('network.json') as file:
    net_config = load(file)

SOLC_COMPILER="/usr/local/bin/solc"

CONTRACT_PATH="contracts/registrar.sol"

### Put your code below this comment ###
